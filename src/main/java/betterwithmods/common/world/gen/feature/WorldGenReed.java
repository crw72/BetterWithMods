package betterwithmods.common.world.gen.feature;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

import java.util.Random;

public class WorldGenReed extends net.minecraft.world.gen.feature.WorldGenReed {

    private enum AllDirections {
        NORTH(new Vec3i(0, 0, -1)),
        NORTHEAST(new Vec3i(1, 0, -1)),
        EAST(new Vec3i(1, 0, 0)),
        SOUTHEAST(new Vec3i(1, 0, 1)),
        SOUTH(new Vec3i(0, 0, 1)),
        SOUTHWEST(new Vec3i(-1, 0, 1)),
        WEST(new Vec3i(-1, 0, 0)),
        NORTHWEST(new Vec3i(-1, 0, -1));

        private final Vec3i direction;

        AllDirections(Vec3i directionVecIn) {
            this.direction = directionVecIn;
        }

        public Vec3i getDirection() {
            return direction;
        }
    }

    @Override
    public boolean generate(World worldIn, Random rand, BlockPos position)
    {
        for (int i = 0; i < 20; ++i)
        {
            BlockPos blockpos = position.add(rand.nextInt(4) - rand.nextInt(4), 0, rand.nextInt(4) - rand.nextInt(4));

            if (worldIn.isAirBlock(blockpos))
            {
                BlockPos blockpos1 = blockpos.down();
                boolean canPlaceBlock = false;

                for (AllDirections direction : AllDirections.values()) {
                    IBlockState iblockstate = worldIn.getBlockState(blockpos1.add(direction.getDirection()));

                    if(iblockstate.getMaterial() == Material.WATER || iblockstate.getBlock() == Blocks.FROSTED_ICE) {
                        canPlaceBlock = true;
                        break;
                    }
                }

                if (canPlaceBlock)
                {
                    int j = 2 + rand.nextInt(rand.nextInt(3) + 1);

                    for (int k = 0; k < j; ++k)
                    {
                        if (Blocks.REEDS.canBlockStay(worldIn, blockpos))
                        {
                            worldIn.setBlockState(blockpos.up(k), Blocks.REEDS.getDefaultState(), 2);
                        }
                    }
                }
            }
        }
        return true;
    }
}
