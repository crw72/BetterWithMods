package betterwithmods.common.world;

import betterwithmods.common.world.gen.village.AbandonedVillagePiece;
import betterwithmods.common.world.gen.village.Well;
import betterwithmods.common.world.gen.village.field.BWFieldBase;
import betterwithmods.module.GlobalConfig;
import betterwithmods.module.hardcore.world.HCVillages;
import betterwithmods.util.ReflectionLib;
import com.google.common.collect.Lists;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.gen.structure.MapGenVillage;
import net.minecraft.world.gen.structure.StructureComponent;
import net.minecraft.world.gen.structure.StructureStart;
import net.minecraft.world.gen.structure.StructureVillagePieces;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static net.minecraft.world.gen.structure.StructureVillagePieces.Road;
import static net.minecraft.world.gen.structure.StructureVillagePieces.Torch;

/**
 * Created by primetoxinz on 5/21/17.
 */
public class BWMapGenVillage extends MapGenVillage {
    public static List<StructureVillagePieces.PieceWeight> getPiecesAbandoned(Random random, int size) {
        List<StructureVillagePieces.PieceWeight> list = Lists.<StructureVillagePieces.PieceWeight>newArrayList();
        net.minecraftforge.fml.common.registry.VillagerRegistry.addExtraVillageComponents(list, random, size);

        list.removeIf(pieceWeight -> (pieceWeight).villagePiecesLimit == 0);
        return list.stream().filter(piece -> BWFieldBase.class.isAssignableFrom(piece.villagePieceClass)).collect(Collectors.toList());
    }

    public static List<StructureVillagePieces.PieceWeight> getPiecesSemi(Random random, int size) {
        List<StructureVillagePieces.PieceWeight> list = Lists.<StructureVillagePieces.PieceWeight>newArrayList();
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.House4Garden.class, 4, MathHelper.getInt(random, 2 + size, 4 + size * 2)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.Church.class, 20, MathHelper.getInt(random, 0 + size, 1 + size)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.House1.class, 20, MathHelper.getInt(random, 0 + size, 2 + size)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.WoodHut.class, 3, MathHelper.getInt(random, 2 + size, 5 + size * 3)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.Hall.class, 15, MathHelper.getInt(random, 0 + size, 2 + size)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.House2.class, 15, MathHelper.getInt(random, 0, 1 + size)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.House3.class, 8, MathHelper.getInt(random, 0 + size, 3 + size * 2)));

        net.minecraftforge.fml.common.registry.VillagerRegistry.addExtraVillageComponents(list, random, size);

        list.removeIf(pieceWeight -> (pieceWeight).villagePiecesLimit == 0);
        return list.stream().filter(piece -> AbandonedVillagePiece.class.isAssignableFrom(piece.villagePieceClass) || Well.class.isAssignableFrom(piece.villagePieceClass)).collect(Collectors.toList());
    }

    public static List<StructureVillagePieces.PieceWeight> getPiecesNormal(Random random, int size) {
        List<StructureVillagePieces.PieceWeight> list = Lists.<StructureVillagePieces.PieceWeight>newArrayList();
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.House4Garden.class, 4, MathHelper.getInt(random, 2 + size, 4 + size * 2)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.Church.class, 20, MathHelper.getInt(random, 0 + size, 1 + size)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.House1.class, 20, MathHelper.getInt(random, 0 + size, 2 + size)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.WoodHut.class, 3, MathHelper.getInt(random, 2 + size, 5 + size * 3)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.Hall.class, 15, MathHelper.getInt(random, 0 + size, 2 + size)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.Field1.class, 3, MathHelper.getInt(random, 1 + size, 4 + size)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.Field2.class, 3, MathHelper.getInt(random, 2 + size, 4 + size * 2)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.House2.class, 15, MathHelper.getInt(random, 0, 1 + size)));
        list.add(new StructureVillagePieces.PieceWeight(StructureVillagePieces.House3.class, 8, MathHelper.getInt(random, 0 + size, 3 + size * 2)));
        net.minecraftforge.fml.common.registry.VillagerRegistry.addExtraVillageComponents(list, random, size);

        list.removeIf(pieceWeight -> (pieceWeight).villagePiecesLimit == 0);
        return list.stream().filter(piece -> !AbandonedVillagePiece.class.isAssignableFrom(piece.villagePieceClass) && !Well.class.isAssignableFrom(piece.villagePieceClass)).collect(Collectors.toList());
    }

    public int getSize() {
        return ObfuscationReflectionHelper.getPrivateValue(MapGenVillage.class, this, ReflectionLib.MAP_GEN_VILLAGE_SIZE);
    }

    @Override
    protected StructureStart getStructureStart(int chunkX, int chunkZ) {
        VillageStatus status = VillageStatus.getStatus(this.world, chunkX, chunkZ);
        if (GlobalConfig.debug)
            System.out.printf("Found %s village @ <%s,%s> : /tp %s ~ %s\n", status.name().toLowerCase(), chunkX, chunkZ, chunkX * 16, chunkZ * 16);
        return new AbandonedStart(this.world, this.rand, chunkX, chunkZ, getSize(), status);
    }

    public enum VillageStatus {
        NORMAL(HCVillages.normalRadius.get()),
        SEMIABANDONED(HCVillages.semiabandonedRadius.get()),
        ABANDONED(0);
        public static final VillageStatus[] VALUES = values();
        private int radius;

        VillageStatus(int radius) {
            this.radius = radius;
        }

        public static VillageStatus getStatus(World worldIn, int chunkX, int chunkZ) {
            for (VillageStatus status : VALUES) {
                if (status.inRadius(worldIn, chunkX * 16, chunkZ * 16 ))
                    return status;
            }
            return NORMAL;
        }

        public boolean inRadius(World world, int x, int z) {
            BlockPos center = world.getSpawnPoint();
            int distFromSpawn = (int) Math.sqrt(Math.pow(x - center.getX(), 2) + Math.pow(z - center.getZ(), 2));

            if (GlobalConfig.debug)
                System.out.printf("Checking for %s village @ (%s,%s) d:%s r:%s\n", this.name(), x, z, distFromSpawn, radius);
            return distFromSpawn >= radius;
        }
    }

    public static class AbandonedStart extends StructureStart {
        private boolean hasMoreThanTwoComponents;
        private VillageStatus status;

        public AbandonedStart() {
        }

        public AbandonedStart(World worldIn, Random rand, int x, int z, int size, VillageStatus status) {
            super(x, z);
            this.status = status;
            List<StructureVillagePieces.PieceWeight> pieceWeights;
            switch (status) {
                case NORMAL:
                    pieceWeights = getPiecesNormal(rand, size);
                    break;
                case SEMIABANDONED:
                    pieceWeights = getPiecesSemi(rand, size);
                    break;
                default:
                    pieceWeights = getPiecesAbandoned(rand, size);
                    break;
            }
            Well start = new Well(status, worldIn.getBiomeProvider(), 0, rand, (x << 4) + 2, (z << 4) + 2, pieceWeights, size);
            this.components.add(start);
            start.buildComponent(start, this.components, rand);
            List<StructureComponent> roads = start.pendingRoads;
            List<StructureComponent> houses = start.pendingHouses;
            while (!roads.isEmpty()) {
                int j = rand.nextInt(roads.size());
                StructureComponent road = roads.remove(j);
                road.buildComponent(start, this.components, rand);
            }
            while (!houses.isEmpty()) {
                int i = rand.nextInt(houses.size());
                StructureComponent house = houses.remove(i);
                if (house instanceof Torch)
                    continue;
                house.buildComponent(start, this.components, rand);
            }

            this.updateBoundingBox();
            int k = 0;

            for (StructureComponent piece : this.components) {
                if (!(piece instanceof Road)) {
                    ++k;
                }
            }

            this.hasMoreThanTwoComponents = k > 2;

        }

        /**
         * currently only defined for Villages, returns true if Village has more than 2 non-road components
         */
        public boolean isSizeableStructure() {
            return this.hasMoreThanTwoComponents;
        }

        public void writeToNBT(NBTTagCompound tagCompound) {
            super.writeToNBT(tagCompound);
            tagCompound.setBoolean("Valid", this.hasMoreThanTwoComponents);
            tagCompound.setInteger("Status", this.status.ordinal());
        }

        public void readFromNBT(NBTTagCompound tagCompound) {
            super.readFromNBT(tagCompound);
            this.hasMoreThanTwoComponents = tagCompound.getBoolean("Valid");
            this.status = VillageStatus.VALUES[tagCompound.getInteger("Status")];
        }
    }

}


