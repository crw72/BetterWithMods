package betterwithmods.common.blocks;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockSandSlab extends BlockSimpleSlab {

    public BlockSandSlab () {
        super(Material.SAND);
        this.setSoundType(SoundType.SAND);
        this.setHardness(0.5F);
        this.setDefaultState(this.blockState.getBaseState());
        this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
        this.setHarvestLevel("shovel", 0);
    }

}

