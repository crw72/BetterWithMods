package betterwithmods.common.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

public class BlockReed extends net.minecraft.block.BlockReed {

    public BlockReed() {
        super();
        setHardness(0.0F);
        setSoundType(SoundType.PLANT);
    }

    private enum AllDirections {
        NORTH(new Vec3i(0, 0, -1)),
        NORTHEAST(new Vec3i(1, 0, -1)),
        EAST(new Vec3i(1, 0, 0)),
        SOUTHEAST(new Vec3i(1, 0, 1)),
        SOUTH(new Vec3i(0, 0, 1)),
        SOUTHWEST(new Vec3i(-1, 0, 1)),
        WEST(new Vec3i(-1, 0, 0)),
        NORTHWEST(new Vec3i(-1, 0, -1));

        private final Vec3i direction;

        AllDirections(Vec3i directionVecIn) {
            this.direction = directionVecIn;
        }

        public Vec3i getDirection() {
            return direction;
        }
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
        IBlockState state = worldIn.getBlockState(pos.down());
        Block block = state.getBlock();

        if (block.canSustainPlant(state, worldIn, pos.down(), EnumFacing.UP, this)) {
            return true;
        } else if (block == this) {
            return true;
        } else if (block != Blocks.GRASS && block != Blocks.DIRT && block != Blocks.SAND) {
            return false;
        } else {
            BlockPos blockpos = pos.down();

            for (AllDirections direction : AllDirections.values()) {
                IBlockState iblockstate = worldIn.getBlockState(blockpos.add(direction.getDirection()));

                if(iblockstate.getMaterial() == Material.WATER || iblockstate.getBlock() == Blocks.FROSTED_ICE) {
                    return true;
                }
            }
            return false;
        }
    }
}
