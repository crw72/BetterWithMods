package betterwithmods.common.blocks;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public class BlockGravelSlab extends BlockSimpleSlab {

    public BlockGravelSlab() {
        super(Material.SAND);
        this.setSoundType(SoundType.GROUND);
        this.setHardness(0.5F);
        this.setDefaultState(this.blockState.getBaseState());
        this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
        this.setTickRandomly(false);
        this.setHarvestLevel("shovel", 0);
    }

    @Override
    public MapColor getMapColor(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        return MapColor.STONE;
    }


/*
    @Override
    public boolean getUseNeighborBrightness(IBlockState state) {
        return true;
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(this, 1, 0);
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return super.getBoundingBox(state, source, pos);
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
        return isOverSupport(worldIn, pos);
    }

    private boolean isOverSupport(World worldIn, BlockPos pos) {
        IBlockState state = worldIn.getBlockState(pos.down());
        return state.getBlock() == Blocks.GRASS_PATH || state.isSideSolid(worldIn, pos.down(), EnumFacing.UP)
                || state.getBlock().canPlaceTorchOnTop(state, worldIn, pos.down())
                || state.isNormalCube()
                || state.isFullCube()
                || state.isFullBlock();
    }

    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos other) {
        if (!isOverSupport(worldIn, pos)) {
            worldIn.destroyBlock(pos, false);
            spawnAsEntity(worldIn, pos, new ItemStack(BWMItems.GRAVEL_PILE));
        }
    }

    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        boolean snowy = false;
        for (EnumFacing facing : CHECKED_FACINGS_FOR_SNOW) {
            BlockPos checkedPos = pos.offset(facing);
            Block block = worldIn.getBlockState(checkedPos).getBlock();
            Block blockOver = worldIn.getBlockState(checkedPos.up()).getBlock();
            if (block == Blocks.SNOW || block == Blocks.SNOW_LAYER ||
                    blockOver == Blocks.SNOW || blockOver == Blocks.SNOW_LAYER) {
                snowy = true;
            }
        }
        state = state.withProperty(SNOWY, snowy);
        return state;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT_MIPPED;
    }

    @Override
    public SoundType getSoundType(IBlockState state, World world, BlockPos pos, @Nullable Entity entity) {
        return this.blockSoundType;
    }
 */
}