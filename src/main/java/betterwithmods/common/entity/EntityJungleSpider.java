package betterwithmods.common.entity;

import betterwithmods.BWMod;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.monster.EntityCaveSpider;
import net.minecraft.entity.monster.EntityIronGolem;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootTableList;

import javax.annotation.Nullable;
import java.util.ArrayList;

public class EntityJungleSpider extends EntityCaveSpider {
    public static final ResourceLocation LOOT = LootTableList.register(new ResourceLocation(BWMod.MODID, "entity/jungle_spider"));

    public EntityJungleSpider(World worldIn) {
        super(worldIn);
    }

    @Override
    public boolean attackEntityAsMob(Entity entityIn) {
        if (super.attackEntityAsMob(entityIn))
        {
            if (entityIn instanceof EntityLivingBase)
            {
                int duration = 0;

                if (this.world.getDifficulty() == EnumDifficulty.NORMAL)
                {
                    duration = 7;
                }
                else if (this.world.getDifficulty() == EnumDifficulty.HARD)
                {
                    duration = 15;
                }

                if (duration > 0)
                {
                    ((EntityLivingBase)entityIn).addPotionEffect(new PotionEffect(MobEffects.HUNGER, duration * 20, 0));
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    @Nullable
    @Override
    protected ResourceLocation getLootTable() {
        return LOOT;
    }

    @Override
    protected boolean isValidLightLevel() {
        return true;
    }

    @Override
    public float getBlockPathWeight(BlockPos pos) {
        return 0.5F;
    }

    @Override
    public boolean getCanSpawnHere() {
        BlockPos pos = new BlockPos(this.posX, this.getEntityBoundingBox().minY, this.posZ);
        BlockPos topPos = world.getHeight(pos).down();
        IBlockState topBlock = world.getBlockState(topPos);

        //On the ground, search up a little, just so we can also spawn in caves
        ArrayList<Integer> possible_spawns = getPossibleSpawnHeights(new BlockPos.MutableBlockPos(pos.up(32)),32);
        //Otherwise, if there's a leaf canopy search from there
        if(possible_spawns.isEmpty() && topBlock.getBlock() == Blocks.LEAVES && topBlock.getValue(BlockLeaves.DECAYABLE)) {
            possible_spawns = getPossibleSpawnHeights(new BlockPos.MutableBlockPos(topPos), 16);
        }
        if(possible_spawns.isEmpty())
            return false;
        this.setPosition(posX, possible_spawns.get(rand.nextInt(possible_spawns.size())), posZ);
        return super.getCanSpawnHere();
    }

    private ArrayList<Integer> getPossibleSpawnHeights(BlockPos.MutableBlockPos pos, int limit)
    {
        ArrayList<Integer> heights = new ArrayList<>();
        int leaves = 0;
        for(int i = 0; i < limit; i++)
        {
            IBlockState state = world.getBlockState(pos);
            if(isJungleLeaves(state))
                leaves += 1;
            else
                leaves = 0;
            if(leaves == 2) //Only on top of leaves that are two blocks thick.
                heights.add(pos.getY()+2);
            pos.move(EnumFacing.DOWN);
        }
        return heights;
    }

    private boolean isJungleLeaves(IBlockState state)
    {
        return state.getBlock() == Blocks.LEAVES && state.getValue(BlockLeaves.DECAYABLE);
    }

    @Override
    protected void initEntityAI()
    {
        this.tasks.addTask(1, new EntityAISwimming(this));
        this.tasks.addTask(3, new EntityAILeapAtTarget(this, 0.4F));
        this.tasks.addTask(4, new EntityJungleSpider.AISpiderAttack(this));
        this.tasks.addTask(5, new EntityAIWanderAvoidWater(this, 0.8D));
        this.tasks.addTask(6, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
        this.tasks.addTask(6, new EntityAILookIdle(this));
        this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, false, new Class[0]));
        this.targetTasks.addTask(2, new EntityJungleSpider.AISpiderTarget(this, EntityPlayer.class));
        this.targetTasks.addTask(3, new EntityJungleSpider.AISpiderTarget(this, EntityIronGolem.class));
    }

    static class AISpiderAttack extends EntityAIAttackMelee
    {
        public AISpiderAttack(EntitySpider spider)
        {
            super(spider, 1.0D, true);
        }

        /**
         * Returns whether an in-progress EntityAIBase should continue executing
         */
        public boolean shouldContinueExecuting()
        {
            if (this.attacker.getRNG().nextInt(100) == 0)
            {
                this.attacker.setAttackTarget((EntityLivingBase)null);
                return false;
            }
            else
            {
                return super.shouldContinueExecuting();
            }
        }

        protected double getAttackReachSqr(EntityLivingBase attackTarget)
        {
            return (double)(4.0F + attackTarget.width);
        }
    }

    static class AISpiderTarget<T extends EntityLivingBase> extends EntityAINearestAttackableTarget<T>
    {
        public AISpiderTarget(EntitySpider spider, Class<T> classTarget)
        {
            super(spider, classTarget, true);
        }

        /**
         * Returns whether the EntityAIBase should begin execution.
         */
        public boolean shouldExecute()
        {
            return super.shouldExecute();
        }
    }
}




