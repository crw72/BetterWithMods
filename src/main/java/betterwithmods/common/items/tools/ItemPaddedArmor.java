package betterwithmods.common.items.tools;

import betterwithmods.client.BWCreativeTabs;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraftforge.common.util.EnumHelper;

public class ItemPaddedArmor extends BWMArmor {
    private static final ArmorMaterial PADDING = EnumHelper.addArmorMaterial("padding", "betterwithmods:padding", 8, new int[]{1, 2, 3, 1}, 0, SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0.0F);

    public ItemPaddedArmor(EntityEquipmentSlot equipmentSlotIn) {
        super(PADDING, equipmentSlotIn, "padding");
        this.setCreativeTab(BWCreativeTabs.BWTAB);
    }
}
